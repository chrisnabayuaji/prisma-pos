<div id="modal_discount" class="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Diskon</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Harga</label>
          <input class="form-control num autonumeric" name="tx_total_discount" id="tx_total_discount" type="text" value="">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
        <button type="button" class="btn btn-info" onclick="edit_discount()"><i class="fa fa-plus"></i> Tambah</button>
      </div>
    </div>
  </div>
</div>